// <auto-generated />
namespace Itldetailing.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class UserforCMS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UserforCMS));
        
        string IMigrationMetadata.Id
        {
            get { return "201705221442171_UserforCMS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
