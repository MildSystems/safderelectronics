﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Itldetailing.Startup))]
namespace Itldetailing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
