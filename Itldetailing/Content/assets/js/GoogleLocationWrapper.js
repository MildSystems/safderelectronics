﻿var GoogleLocationWrapper = (function () {
    var init = function (selector) {
        var autocomplete = new google.maps.places.Autocomplete($('#'+selector)[0], {});

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            console.log(place.address_components);
        });
    }
    return {
        init: init
    }
})();