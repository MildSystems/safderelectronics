

// // $(".photo").click(function(){
// //  $(this).next().addClass("block-y");
// // });
// // $(".press").click(function(){
// //     $(".photo-detail-panel").removeClass("block-y");
// // });
// // $(".nav-button").click(function(){
// //     $(".nav-button").toggleClass("active");
// // });




 function sticky_header() {

     "use strict";
     $(window).scroll(function() {
         if ($(this).scrollTop() > 100){  
             $('#main-navigation').addClass("sticky");
         }
         else{
             $('#main-navigation').removeClass("sticky");
         }
         if ($(this).scrollTop() > 150) {
             $('.scrollToTop') .addClass('left') .removeClass('right');
         } else {
             $('.scrollToTop') .addClass('right') .removeClass('left');
         }
     });

 };
 function gototop() {
     "use strict";
     //Click event to scroll to top
     $('.scrollToTop').on("click", function(){
         $('html, body').animate({scrollTop : 0},800);
         return false;
     });
 };

 jQuery(document).ready(function($) {
     sticky_header();
     gototop();
 });
