﻿$(document).ready(function () {
    loadPartialView();
    showAdd('hide');
});

var loadPartialView = function (pageno) {
    var getUrl = $("#hidPartialViewUrl").val();


    $.ajax({
        url: getUrl,
        type: "POST",
        data: {},
        success: function (msg) {
            $("#divPartialView").html(msg);
        },
        error: function () {

        }
    });
}


var showAdd = function (sh) {
    if (sh === "show") {
        $("#divAdd").hide();
        $("#divClose").show();
        $("#divHide").slideDown();
    }
    else {
        $("#divClose").hide();
        $("#divAdd").show();
        $("#divHide").slideUp();
        clears();
    }
}

var clears = function () {
    $("#hidId").val("");
    $("#role").val("");
    $("#divClose").hide();
    $("#divAdd").show();
    $("#divHide").slideUp();
}

var editOfficeRole = function (id, name) {
    $("#divAdd").hide();
    $("#divClose").show();
    $("#divHide").slideDown();
    $('html, body').animate({
        scrollTop: $("#divHide").offset().top - 200
    }, 1000);
    $("#hidId").val(id);
    $("#role").val(name);
}

var save = function () {
    var getUrl = $("#hidSaveRoleUrl").val();
    var name = $("#role").val();
    var er = 0;
    if (name == "") {
        $("#role").css("border", "1px solid red");
        er = er + 1;
    }
    else {
        $("#role").css("border", "1px solid #ccc");
    }
    if (er == 0) {
        $.ajax({
            url: getUrl,
            type: "POST",
            data: { nameParam: name + "" },
            success: function (response) {
                var message;
                if (response === "true") {
                    //AutoLoader("Record Saved Successfully...!", "success");
                    loadPartialView();
                    clears();
                } else {
                    message = "Role is already added....";
                }
            },
            error: function () {
            }
        });
    }
}

var deleteRole = function (name) {
    var getUrl = $("#hidDeleteRoleUrl").val();
    var aa = confirm("Are You Sure You Want To Delete?");
    if (aa) {
        $.ajax({
            url: getUrl,
            type: "POST",
            data: { nameParam: name + "" },
            success: function (response) {
                //AutoLoader("Record delete Successfully...!", "success");
                loadPartialView();
            },
            error: function () {
            }
        });
    }
}
