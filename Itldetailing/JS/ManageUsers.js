﻿$(document).ready(function () {
    loadPartialView();
});

var loadPartialView = function () {
    var getUrl = $("#hidPartialViewUrl").val();


    $.ajax({
        url: getUrl,
        type: "POST",
        data: {},
        success: function (msg) {
            $("#divPartialView").html(msg);
        },
        error: function () {

        }
    });
};