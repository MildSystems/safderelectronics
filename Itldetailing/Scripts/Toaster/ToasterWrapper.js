﻿/// <reference path="Toaster/toastr.min.js" />

var ToasterWrapper = (function () {
    var getOption = function()
    {
        return {
            showInfo: false
        , showWarning: false
        , showError: false
        ,success:false
        , Message: ''
        }
    }

    var showToaster = function (options) {
        if (options.showInfo) {
            toastr.info(options.Message);
        } else if (options.showWarning) {
            toastr.warning(options.Message);
        }
        else if (options.showError) {
            toastr.error(options.Message);
        }
        else if (options.success) {
            toastr.success(options.Message);
        }
    }



    return {
        showToaster: showToaster
        ,   getOption:getOption

    }

})();