﻿/// <reference path="../../External/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js" />

var BootstrapEditorWrapper = (function () {
    var init = function (selector) {
        if (typeof selector==='object') {
            selector.wysihtml5();
        } else if (typeof selector==='string') {
            $('#' + selector).wysihtml5();
        }
    }

    var setValue = function (selector, value) {
        
        if (typeof selector === 'object') {
            $('.wysihtml5-sandbox').contents().find('body').html(value);
        }
        else if (typeof selector === 'string') {
            $('.wysihtml5-sandbox').contents().find('body').html(value);
        }
    }
    return {
        init: init,
        setValue:setValue
    }
})();