﻿var Custom = (function () {
    var initEditor = function (selector) {
        BootstrapEditorWrapper.init(selector);
    }

    var SetValue = function (Selector, Value) {
        BootstrapEditorWrapper.setValue(Selector, Value);
    }


    return {
        initEditor: initEditor,
        SetValue: SetValue
    }
})();