﻿var Services = (function () {
    var compare = [];
    var init= function() {
        $('.custom-service').off('click', serviceClick).on('click', serviceClick);
        $('#btnCompare').off('click', btnCompareClick).on('click', btnCompareClick);
    }

    var btnCompareClick = function () {
        if (compare.length === 2) {
            var id = compare[0] + '_' + compare[1];
            window.location.href = '/Compares/Index?Id=' + id;
        } else {
            var option = ToasterWrapper.getOption();
            option.showInfo = true;
            option.Message = 'Please select 2 services to compare';
            ToasterWrapper.showToaster(option);
        }
    }



    var serviceClick = function () {
        if (compare.length < 2) {
            var id = $(this).attr('id');
            compare.push(id);
            $(this).find('.srvce-info-inner').css('border-color','#9de147');
        } else {
            var option = ToasterWrapper.getOption();
            option.showInfo = true;
            option.Message = 'You can not select more than two services';
            ToasterWrapper.showToaster(option);
            $('.custom-service').find('.srvce-info-inner').attr('style','border-color: #fff');
            compare = [];
        }
        
    }
    return {
        init: init
    }
})();