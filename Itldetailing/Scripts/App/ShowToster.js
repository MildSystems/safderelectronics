﻿var ShowToster = (function () {
   
    var init = function () {
      
        //$('#submit1').off('click', btnRequestClick).on('click', btnRequestClick);
        //$('#submit2').off('click', btnScheduleClick).on('click', btnScheduleClick);
    }

    var btnRequestClick = function () {
      
            var option = ToasterWrapper.getOption();
            option.showInfo = true;
            option.Message = 'Request a Call back form has been submited';
            ToasterWrapper.showToaster(option);
       
    }
    var btnScheduleClick = function () {

        var option = ToasterWrapper.getOption();
        option.showInfo = true;
        option.Message = 'Schedule An Appoinment Form Has been submited';
        ToasterWrapper.showToaster(option);

    }

    
    return {
        init: init
    }
})();