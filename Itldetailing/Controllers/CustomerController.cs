﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Data.Entity;

namespace Itldetailing.Controllers
{
    public class CustomerController : BaseController
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();   
        // GET: Customer
        public ActionResult Index()
        {
            ViewBag.Group = db.ReferenceGroups.OrderBy(x=>x.GroupName).ToList();
            ViewBag.Employee = db.Employes.OrderBy(x=>x.EmployeName).ToList();
            return View(new CustmerViewModel());
        }
       
        public ActionResult Save(CustmerViewModel model)
        {
            if (ModelState!= null)
            {
                try
                {
                    Customer m1 = new Customer();
                    m1.CustomerStatus = model.cstmr.CustomerStatus;
                    m1.CustomeCode = model.cstmr.CustomeCode;
                    m1.CustomerName = model.cstmr.CustomerName;
                    m1.CustomerFather = model.cstmr.CustomerFather;
                    m1.CustomerAdress1 = model.cstmr.CustomerAdress1;
                    m1.CustomerAdress2 = model.cstmr.CustomerAdress2;
                    m1.CustomerCnic = model.cstmr.CustomerCnic;
                    m1.CustomerHomeType = model.cstmr.CustomerHomeType;
                    m1.CustomerPhone = model.cstmr.CustomerPhone;
                    m1.CustomerMobile = model.cstmr.CustomerMobile;
                    m1.CustomerArea = model.cstmr.CustomerArea;
                    m1.CustomerIncome = model.cstmr.CustomerIncome;
                    m1.CustomerProfession = model.cstmr.CustomerProfession;
                    m1.Collector = model.cstmr.Collector;
                    m1.CollectionMode = model.cstmr.CollectionMode;
                    m1.Fk_ReferenceGroup = model.cstmr.Fk_ReferenceGroup;
                    m1.EnableFalg = true;
                    m1.CreatedBy = GetUser().UserName;
                    m1.CreatedDate = DateTime.UtcNow.AddHours(5);

                    db.Customers.Add(m1);
                    db.SaveChanges();

                    Customer_Verification cv = new Customer_Verification();
                    cv.VerifyBy = model.cstrVari.VerifyBy;
                    cv.ReferenceGroup = model.cstmr.Fk_ReferenceGroup;
                    cv.Remarks = model.cstrVari.Remarks;
                    cv.Customertype = model.cstrVari.Customertype;
                    cv.VerifyResult = model.cstrVari.VerifyResult;
                    cv.Fk_Customer = m1.id;
                    cv.Fk_Employe = model.cstrVari.Fk_Employe;

                    db.Customer_Verification.Add(cv);
                    db.SaveChanges();

                    foreach (var i in model.grnter)
                    {
                        Grentter g = new Grentter();
                        g.GrentterName = i.GrentterName;
                        g.GrentterFather = i.GrentterFather;
                        g.GrentterAddress = i.GrentterAddress;
                        g.GrentterPhone = i.GrentterPhone;
                        g.GrentterMobile = i.GrentterMobile;
                        g.GrentterCnic = i.GrentterCnic;
                        g.GrentterHomeType = i.GrentterHomeType;
                        g.GrentterProfession = i.GrentterProfession;
                        g.CreatedBy = GetUser().UserName;
                        g.CreatedDate = DateTime.UtcNow.AddHours(5);
                        g.Fk_Customer = m1.id;
                        g.EnableFalg = true;

                        db.Grentters.Add(g);
                    }
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                }               
            }

            return RedirectToAction("index");
        }
        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.Customers.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
        public ActionResult Partial()
        {
            var model = db.Customers.Where(x => x.EnableFalg == true).ToList();

            if(model == null)
            {
                model = new List<Customer>();
            }

            return PartialView("~/Views/Customer/_CostmorPartial.cshtml",model);
        }
        public ActionResult Edit(int id)
        {

            ViewBag.Group = db.ReferenceGroups.OrderBy(x => x.GroupName).ToList();
            ViewBag.Employee = db.Employes.OrderBy(x => x.EmployeName).ToList();

            var model = new CustmerViewModel();

            model.cstmr = db.Customers.Find(id);
            model.grnter = db.Grentters.Where(x=>x.Fk_Customer == id).ToList();
            model.cstrVari = db.Customer_Verification.FirstOrDefault(x => x.Fk_Customer == id);

            return View(model);
        }
        [HttpGet]
        public ActionResult Dtl(int id)
        {
            var model = db.Customers.Find(id);
            if (model == null)
            {
                model = new Customer();
            }

            return View(model);
        }
    }
}
