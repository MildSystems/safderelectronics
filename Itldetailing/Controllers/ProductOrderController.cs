﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Itldetailing.Controllers
{
    public class ProductOrderController : Controller
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();
        // GET: Supplier
        public ActionResult Index()
        {
            ViewBag.PO = db.Product_Order.Where(x => x.EnableFalg == true).ToList();
            return View(new Product_Order());
        }
        public ActionResult Save(Product_Order model)
        {
            if (model != null)
            {
                try
                {
                    model.EnableFalg = true;
                    model.CreatedDate = DateTime.UtcNow.AddHours(5);
                    db.Product_Order.Add(model);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return RedirectToAction("index");
        }

        private object GetUser()
        {
            throw new NotImplementedException();
        }

        public ActionResult Partial()
        {
            {
                var model = db.Product_Order.Where(x => x.EnableFalg == true).ToList();

                if (model == null)
                {
                    model = new List<Product_Order>();
                }

                return PartialView("~/Views/ProductOrder/Partial.cshtml", model);
            }

        }
        public ActionResult Edit(int id)
        {
            ViewBag.M = db.Product_Order.Where(x => x.EnableFalg == true).ToList();

            var model = db.Product_Order.Find(id);
            return View(model);
        }

        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.Product_Order.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
    }

}