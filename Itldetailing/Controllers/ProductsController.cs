﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itldetailing.Controllers
{
    public class ProductsController : BaseController
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();
        // GET: Product
        public ActionResult Index()
        {
            ViewBag.M = db.Products.Where(x => x.EnableFalg == true).ToList();
            return View(new Product());
        }
        [HttpPost]
        public ActionResult Save(String Status,String ProductCode,String Name, String Model, 
            String Description,String Company,String Category,String ProductType, int? Id, DateTime? CreatedDate, string CreatedBy)
        {
            Product model = new Product();
            try
            {
                model.Status = Status;
                model.ProductCode = ProductCode;
                model.Name = Name;
                model.Model = Model;
                model.Description = Description;
                model.Company = Company;
                model.Category = Category;
                model.ProductType = ProductType;
                model.EnableFalg = true;
                model.id = Convert.ToInt32(Id);

                if (model.id == 0)
                {
                    model.CreatedBy = GetUser().UserName;
                    model.CreatedDate = DateTime.UtcNow.AddHours(5);
                }
                else
                {
                    model.UpdatedBy = GetUser().UserName;
                    model.UpdatedDate = DateTime.UtcNow.AddHours(5);
                }


                db.Entry(model).State = model.id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("index");
        }
        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.Products.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
        public ActionResult Partial()
        {
            var model = db.Products.Where(x => x.EnableFalg == true).ToList();

            if (model == null)
            {
                model = new List<Product>();
            }
            return PartialView("~/Views/Products/Partial.cshtml", model);
        }
        public ActionResult ProductEdit(int id)
        {
            ViewBag.M = db.Products.Where(x => x.EnableFalg == true).ToList();

            var model = db.Products.Find(id);
            return View(model);
        }
    }
}