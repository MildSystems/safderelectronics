﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Itldetailing.Controllers
{
    public class SupplierController : BaseController
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();
        // GET: Supplier
        public ActionResult Index()
        {
            ViewBag.S = db.Suppliers.Where(x => x.EnableFalg == true).ToList();
            return View();
        }
        public ActionResult Save(Supplier model)
        {
            if (model != null)
            {
                try
                {
                    model.EnableFalg = true;
                    //model.CreatedBy = GetUser();
                    model.CreatedDate = DateTime.UtcNow.AddHours(5);
                    db.Suppliers.Add(model);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return RedirectToAction("index");
        }

        private object GetUser()
        {
            throw new NotImplementedException();
        }

        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.Suppliers.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
        public ActionResult Partial()
        {
            {
                var model = db.Suppliers.Where(x => x.EnableFalg == true).ToList();

                if (model == null)
                {
                    model = new List<Supplier>();
                }

                return PartialView("~/Views/Supplier/SupplierPartial.cshtml", model);
            }
            
        }
        public ActionResult Edit(int id)
        {
            ViewBag.M = db.Suppliers.Where(x => x.EnableFalg == true).ToList();

            var model = db.Suppliers.Find(id);
            return View(model);
        }
    }
    
}