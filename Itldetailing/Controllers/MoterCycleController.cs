﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Itldetailing.Controllers
{
    public class MoterCycle1Controller : BaseController
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();
        // GET: MoterCycle
        public ActionResult Index()
        {
            ViewBag.M = db.Products.Where(x => x.EnableFalg == true).ToList();
            return View(new MoterCycle());
        }
        
        public ActionResult Save(string Make, string Model, string Cdi, string EngineNo
                                 , string ChaiseNo, string Color, string Key, string Code,
                                int? Fk_Product, int? Id, DateTime? CreatedDate,string CreatedBy)
        {
            MoterCycle model = new MoterCycle();
            try
            {
                model.Make = Make;
                model.Model = Model;
                model.Cdi = Cdi;
                model.EngineNo = EngineNo;
                model.ChaiseNo = ChaiseNo;
                model.Color = Color;
                model.Key = Key;
                model.Code = Code;
                model.Fk_Product = Fk_Product;
                model.EnableFalg = true;
                model.CreatedBy = CreatedBy;
                model.CreatedDate = CreatedDate;
               
                if (model.Id == 0)
                {
                    model.CreatedBy = GetUser().UserName;
                    model.CreatedDate = DateTime.UtcNow.AddHours(5);
                }
                else
                {
                    model.UpdatedBy = GetUser().UserName;
                    model.UpdatedDate = DateTime.UtcNow.AddHours(5);
                }


                db.Entry(model).State = model.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("index");
        }
        public ActionResult Partial()
        {
            var model = db.MoterCycles.Where(x=>x.EnableFalg == true).ToList();

            if (model == null)
            {
                model = new List<MoterCycle>();
            }

            return PartialView("~/Views/MoterCycle1/MoterCyclePartial.cshtml",model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.M = db.Products.Where(x => x.EnableFalg == true).ToList();

            var model = db.MoterCycles.Find(id);
            return View(model);
        }

        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.MoterCycles.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
    }
}