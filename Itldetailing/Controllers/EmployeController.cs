﻿using Itldetailing.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Itldetailing.Controllers 
{
    
    public class EmployeController : BaseController
    {
        SafdarElectronicsEntities db = new SafdarElectronicsEntities();
        // GET: Employe
        public ActionResult Index()
        {
            ViewBag.E = db.Employes.Where(x => x.EnableFalg == true).ToList();
            return View(new Employe());
        }
        public ActionResult Save(Employe model)
        {
            if (model != null)
            {
                try
                {
                    model.EnableFalg = true;
                    model.CreatedBy = GetUser().UserName;
                    model.CreatedDate = DateTime.UtcNow.AddHours(5);
                    db.Employes.Add(model);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }

            return RedirectToAction("index");
        }
        public JsonResult delete(int id)
        {
            string res = "";
            try
            {
                var model = db.Employes.Find(id);
                model.EnableFalg = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                res = "true";
            }
            catch (Exception ex)
            {
                res = "false";
            }
            return Json(res);
        }
        public ActionResult Edit(int id)
        {
            ViewBag.M = db.Employes.Where(x => x.EnableFalg == true).ToList();

            var model = db.Employes.Find(id);
            return View(model);
        }
        public ActionResult Partial()
        {
            var model = db.Employes.Where(x => x.EnableFalg == true).ToList();

            if (model == null)
            {
                model = new List<Employe>();
            }

            return PartialView("~/Views/Employe/EmployePartial.cshtml",model);
        }
    }
}