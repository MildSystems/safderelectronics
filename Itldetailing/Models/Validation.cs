﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itldetailing.Models
{
    [MetadataType(typeof(GrentterValidation))]
    public partial class Grentter
    {
    }
    public class GrentterValidation
    {
        [Required(ErrorMessage = "Grentter Name is Required")]
        public string GrentterName { get; set; }
        [Required(ErrorMessage = "Grentter Father is Required")]
        public string GrentterFather { get; set; }
        [Required(ErrorMessage = "Grentter Address is Required")]
        public string GrentterAddress { get; set; }
        [Required(ErrorMessage = "Grentter Mobile is Required")]
        public string GrentterMobile { get; set; }
        [Required(ErrorMessage = "Grentter Cnic is Required")]
        public string GrentterCnic { get; set; }
        [Required(ErrorMessage = "Grentter Profession By is Required")]
        public string GrentterProfession { get; set; }
    }


    [MetadataType(typeof(Customer_VerificationValidation))]
    public partial class Customer_Verification
    {
    }
    public class Customer_VerificationValidation
    {
        [Required(ErrorMessage = "Verify By is Required")]
        public string VerifyBy { get; set; }
        [Required(ErrorMessage = "Verify Result is Required")]
        public string VerifyResult { get; set; }
        [Required(ErrorMessage = "Employe is Required")]
        public Nullable<int> Fk_Employe { get; set; }
    }

    [MetadataType(typeof(CustomerValidation))]
    public partial class Customer
    {
        
    }
    public class CustomerValidation
    {
        [Required(ErrorMessage = "Custome Code is Required")]
        public Nullable<int> CustomeCode { get; set; }
        [Required(ErrorMessage = "Customer Name is Required")]
        public string CustomerName { get; set; }
        [Required(ErrorMessage = "Custome Father is Required")]
        public string CustomerFather { get; set; }
        [Required(ErrorMessage = "Customer Cnic is Required")]
        public string CustomerAdress1 { get; set; }
        [Required(ErrorMessage = "Customer Code is Required")]
        public string CustomerCnic { get; set; }
        [Required(ErrorMessage = "Customer Mobile is Required")]
        public string CustomerMobile { get; set; }
        [Required(ErrorMessage = "Customer Area is Required")]
        public Nullable<double> CustomerIncome { get; set; }
        [Required(ErrorMessage = "Customer Profession is Required")]
        public string CustomerProfession { get; set; }
        [Required(ErrorMessage = "Collector is Required")]
        public string Collector { get; set; }
        [Required(ErrorMessage = "Collection Mode is Required")]
        public string CollectionMode { get; set; }

    }
}