﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itldetailing.Models
{
    [MetadataType(typeof(ValidationMoterCycle))]    
    public partial class MoterCycle
    {
    }
    public class ValidationMoterCycle
    {
        [Required(ErrorMessage = "Make Is Required")]
        public string Make { get; set; }
        [Required(ErrorMessage = "Model Is Required")]
        public string Model { get; set; }
        [Required(ErrorMessage = "Cdi Is Required")]
        public string Cdi { get; set; }
        [Required(ErrorMessage = "Engine No Is Required")]
        public string EngineNo { get; set; }
        [Required(ErrorMessage = "Chaise No Is Required")]
        public string ChaiseNo { get; set; }
        [Required(ErrorMessage = "Color Is Required")]
        public string Color { get; set; }
        //[Required(ErrorMessage = " ")]
        //public string Key { get; set; }
        [Required(ErrorMessage = "Code Is Required")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Product Is Required")]
        public Nullable<int> Fk_Product { get; set; }
    }
    [MetadataType(typeof(ValidationEmp))]
    public partial class Employe
    {

    }
    public class ValidationEmp
    {
        [Required(ErrorMessage = "Name is Req")]
        public String EmployeName { get; set; }
        [Required(ErrorMessage = "Employe Code Is Req")]
        public String Employecode { get; set; }
        [Required(ErrorMessage = "Employe Father Is Req")]
        public String EmployeFather { get; set; }
        [Required(ErrorMessage = "Employe Designation Is Req")]
        public String EmployeDesignation { get; set; }
        [Required(ErrorMessage = "Employe Address Is Req")]
        public String EmployeAddress { get; set; }
        [Required(ErrorMessage = "Mobile numer Is Req")]
        public String EmployeMobile { get; set; }
        [Required(ErrorMessage = "CNIC Is Req")]
        public String EmployeCnic { get; set; }
        [Required(ErrorMessage = "Salary Is Req")]
        public int EmployeSalary { get; set; }
        //[Required(ErrorMessage = " ")]
        //public int AdvanceSalary { get; set; }
    }

    [MetadataType(typeof(ValidationSup))]
    public partial class Supplier
    {

    }
    public class ValidationSup
    {
        [Required(ErrorMessage = "Ac.Code is Req")]
        public int AcCode { get; set; }
        [Required(ErrorMessage = "Description Is Req")]
        public String Description { get; set; }
        [Required(ErrorMessage = "Credit Amount Is Req")]
        public int CreditAmount { get; set; }
        [Required(ErrorMessage = "Debit Amount Is Req")]
        public int DebitAmount { get; set; }
        //[Required(ErrorMessage = "Location Is Req")]
        //public String Location { get; set; }
        
    }
    [MetadataType(typeof(ValidationPro))]
    public partial class Product
    {

    }
    public class ValidationPro
    {
        [Required(ErrorMessage = "Status Is Req")]
        public string Status { get; set; }
        [Required(ErrorMessage = "Product Code Is Req")]
        public string ProductCode { get; set; }
        [Required(ErrorMessage = "Name Is Req")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Model Is Req")]
        public string Model { get; set; }
        [Required(ErrorMessage = "Description Is Req")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Company Is Req")]
        public string Company { get; set; }
        [Required(ErrorMessage = "Category Is Req")]
        public string Category { get; set; }
        [Required(ErrorMessage = "Product Type Is Req")]
        public string ProductType { get; set; }

    }
    [MetadataType(typeof(ValidationOrder))]
    public partial class Product_Order
    {

    }
    public class ValidationOrder
    {
        [Required(ErrorMessage = "Quantity Is Req")]
        public Nullable<int> OrderQuantity { get; set; }
            [Required(ErrorMessage = "Pur Rate is Req")]
            public Nullable<int> PurchaseRate { get; set;}
        [Required(ErrorMessage = "Sales Rate is Req")]      
        public Nullable<int> SalesRate { get; set; }
        [Required(ErrorMessage = "Last rate Is Req")]
        public Nullable<int> LastRate { get; set; }
        public Nullable<int> Fk_Product { get; set; }
        public Nullable<int> Fk_MotorCycle { get; set; }
    }
}