﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itldetailing.Models
{
    public class CustmerViewModel
    {
        public CustmerViewModel()
        {
            cstmr = new Customer();
            cstrVari = new Customer_Verification();
            grnter = new List<Grentter>();
            grnter.Add(new Grentter());
            grnter.Add(new Grentter());
        }

        public Customer cstmr { get; set; }
        public Customer_Verification cstrVari { get; set; }
        public List<Grentter> grnter { get; set; }
    }
}